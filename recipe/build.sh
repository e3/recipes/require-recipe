#!/bin/bash

export EPICS_MODULES="${PREFIX}/modules"
export E3_REQUIRE_LOCATION="${EPICS_MODULES}/${PKG_NAME}/${PKG_VERSION}"
export E3_REQUIRE_TOOLS="${E3_REQUIRE_LOCATION}/tools"
mkdir -p "$EPICS_MODULES" "$E3_REQUIRE_TOOLS"

make MODULE=${PKG_NAME} LIBVERSION=${PKG_VERSION}
make MODULE=${PKG_NAME} LIBVERSION=${PKG_VERSION} db_internal
make MODULE=${PKG_NAME} LIBVERSION=${PKG_VERSION} install

# Install require tools
install -m 644 ./require-ess/tools/driver.makefile ${E3_REQUIRE_TOOLS}/
# Install require scripts under ${PREFIX}/bin so they are in the PATH
install -m 755 ./require-ess/tools/iocsh_complete.bash ${PREFIX}/bin/
install -m 755 ./require-ess/tools/iocsh ${PREFIX}/bin/
install -m 755 ./require-ess/tools/iocsh_utils.py ${PREFIX}/bin/

# Create activate/deactivate scripts
mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/require_activate.sh
export EPICS_MODULES="$EPICS_MODULES"
export E3_REQUIRE_NAME="${PKG_NAME}"
export E3_REQUIRE_VERSION="${PKG_VERSION}"
export E3_REQUIRE_LOCATION="${E3_REQUIRE_LOCATION}"
export E3_REQUIRE_TOOLS="${E3_REQUIRE_TOOLS}"
export E3_REQUIRE_BIN="${PREFIX}/bin"
export E3_REQUIRE_LIB="${E3_REQUIRE_LOCATION}/lib"
export E3_REQUIRE_DB="${E3_REQUIRE_LOCATION}/db"
export E3_REQUIRE_DBD="${E3_REQUIRE_LOCATION}/dbd"
export E3_REQUIRE_INC="${E3_REQUIRE_LOCATION}/include"
export E3_REQUIRE_CONFIG="${E3_REQUIRE_LOCATION}/configure"
export EPICS_DRIVER_PATH="${EPICS_MODULES}"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/require_deactivate.sh
unset EPICS_MODULES
unset E3_REQUIRE_NAME
unset E3_REQUIRE_VERSION
unset E3_REQUIRE_LOCATION
unset E3_REQUIRE_TOOLS
unset E3_REQUIRE_BIN
unset E3_REQUIRE_LIB
unset E3_REQUIRE_DB
unset E3_REQUIRE_DBD
unset E3_REQUIRE_INC
unset E3_REQUIRE_CONFIG
unset EPICS_DRIVER_PATH
EOF
