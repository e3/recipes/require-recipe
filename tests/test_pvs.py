import os
import subprocess

from run_iocsh import IOC

PREFIX = os.environ["IOCNAME"] = "PREFIX"

# require publishes five PVs, plus one per loaded module:
# - BaseVersion
# - Modules
# - Versions
# - ModuleVersions
# - Labels  # should be internal; will not be tested
# - ${module}Version


def test_pv_is_populated() -> None:
    with IOC():
        for _property in {
            "BaseVersion",
            "Modules",
            "Versions",
            "ModuleVersions",
            "requireVersion",
        }:
            pv_name = f"{PREFIX}:{_property}"
            rv = subprocess.run(["pvget", pv_name], capture_output=True, text=True)
            value = rv.stdout.strip()  # to remove whitespace
            assert value


def test_modules_contains_only_require_name_when_no_modules() -> None:
    pv_name = f"{PREFIX}:Modules"

    with IOC():
        rv = subprocess.run(["pvget", pv_name], capture_output=True, text=True)
        # the response will look like
        #     PREFIX:Modules 1990-01-01 00:00:00.000  require
        value = rv.stdout.strip().split()[-1]

        assert value == "require"
