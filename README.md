# require conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/require

Package license: GPL2

Recipe license: BSD 3-Clause

Summary: ESS Customized PSI dynamic modules for EPICS
